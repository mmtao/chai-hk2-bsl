# hk2-bsl
A boostrap loader for the HK (STM32F429Z) on the central hub of the MMT/MAPS adaptive secondary mirror. The main communcation interface is the 100 Mb Ethernet connection. LwIP is used. 

## IP

Assumes that the RC is `10.2.0.1` and that the HK should be `10.2.0.2`.  It is highly recommended that the file `/etc/hosts` on the RC include the line `10.2.0.2 hk`.

HK's MAC address is now `6B:65:65:70:65:72`, which is `keeper` in ASCII hex.

## Commands

A (very, very) minimal shell is available over telnet. `telnet hk` gives you access to the following commands:
* `crc` : Verify the CRC of the running BSL.
* `verifys` : Verify the contents of the scratch partition.
* `verifya` : Verify the contents of the app partition.
* `burn` : Verify the scratch partition, burn it to the app partition, and then verify the app partition.
* `quit` : End your telnet session.
* `boot` : Boot the HK2 application.

## Reprogramming

Reprogramming uses Tar, TFTP, and Telnet. If this were an application intended for an open network, this would be very poor security practice. But, the HK is designed only to be used behind a firewall, directly connected to a properly secured server. While this doesn't make it good security practice, it does make it reasonable to compromise.

### Step 1. Take the application binary and wrap in a tar archive.

The embedded software needs to know how big the file is. This is the easiest way, as `tar` is installed on every Linux box. If your application binary is `app.bin`, do this:

```
tar cf app.tar app.bin
```

### Step 2. Upload the tar file to the HK.

The HK BSL contains a TFTP server. To upload your tar file to the HK, do this:

```
tftp -m binary hk -c put app.tar
```

This will take ~3 seconds. The file is written into the scratch partition in the HK flash.

### Step 3. Verify the scratch partition and then burn the image into the app location.

The HK BSL contains a Telnet server. To connect, do `telnet hk`. You can use the command `verifys` to verify the contents of the scratch partition, or just `burn`, which will verify the contents of the scratch partition before copying it to the app partion, and verifying it once it's there. You can also use the command `verifya` to verify the contents of the app partition. 

```
[asg@aorcdev ~]$ telnet hk
Trying 10.2.0.2...
Connected to hk.
Escape character is '^]'.
HK BSL 2
Version 2.0.2
Booted in 1794 ms
$ crc
Expected: b580a058
Computed: b580a058
$ burn
Scratch file: 'hk2-app', 78480 bytes. eCRC=eof99db8
Computed CRC: e0f99db8. Image verified.
Wrote 81920 bytes.
App file: 'hk2-app.bin', 78480 bytes. eCRC=eof99db8
Computed CRC: eof99db8. Image verified.
$
```

## Flash memory map

|  Bank | Name      | Block base address | Size   | Use      | Size |
| ------|-----------|--------------------|--------|----------|------|
|  1    | Sector  0 | 0800_0000          |  16 KB | BSL##### | 256K |
|       | Sector  1 | 0800_4000          |  16 KB | ######## |
|       | Sector  2 | 0800_8000          |  16 KB | ######## |
|       | Sector  3 | 0800_C000          |  16 KB | ######## |
|       | Sector  4 | 0801_0000          |  64 KB | ######## |
|       | Sector  5 | 0802_0000          | 128 KB | ######## |
|       | Sector  6 | 0804_0000          | 128 KB |
|       | Sector  7 | 0806_0000          | 128 KB |
|       | Sector  8 | 0808_0000          | 128 KB |
|       | Sector  9 | 080A_0000          | 128 KB |
|       | Sector 10 | 080C_0000          | 128 KB |
|       | Sector 11 | 080E_0000          | 128 KB |
|  2    | Sector 12 | 0810_0000          |  16 KB | Settings |  16K |
|       | Sector 13 | 0810_4000          |  16 KB |
|       | Sector 14 | 0810_8000          |  16 KB |
|       | Sector 15 | 0810_C000          |  16 KB |
|       | Sector 16 | 0811_0000          |  64 KB |
|       | Sector 17 | 0812_0000          | 128 KB | Scratch# | 384K |
|       | Sector 18 | 0814_0000          | 128 KB | ######## |
|       | Sector 19 | 0816_0000          | 128 KB | ######## |
|       | Sector 20 | 0818_0000          | 128 KB | HK App## | 384K |
|       | Sector 21 | 081A_0000          | 128 KB | ######## |
|       | Sector 22 | 081C_0000          | 128 KB | ######## |
|       | Sector 23 | 081E_0000          | 128 KB | 

## License

![](gpl_v3.png)

Copyright (c) 2022, Arizona Board of Regents on behalf of the University of Arizona. All rights reserved.
