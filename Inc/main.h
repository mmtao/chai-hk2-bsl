/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.h
  * @brief          : Header for main.c file.
  *                   This file contains the common defines of the application.
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2022 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under BSD 3-Clause license,
  * the "License"; You may not use this file except in compliance with the
  * License. You may obtain a copy of the License at:
  *                        opensource.org/licenses/BSD-3-Clause
  *
  ******************************************************************************
  */
/* USER CODE END Header */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __MAIN_H
#define __MAIN_H

#ifdef __cplusplus
extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#include "stm32f4xx_hal.h"
#include "stm32f4xx_hal.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */

/* USER CODE END Includes */

/* Exported types ------------------------------------------------------------*/
/* USER CODE BEGIN ET */

/* USER CODE END ET */

/* Exported constants --------------------------------------------------------*/
/* USER CODE BEGIN EC */

/* USER CODE END EC */

/* Exported macro ------------------------------------------------------------*/
/* USER CODE BEGIN EM */

/* USER CODE END EM */

/* Exported functions prototypes ---------------------------------------------*/
void Error_Handler(void);

/* USER CODE BEGIN EFP */

/* USER CODE END EFP */

/* Private defines -----------------------------------------------------------*/
#define SPI4_ACC_SCK_Pin GPIO_PIN_2
#define SPI4_ACC_SCK_GPIO_Port GPIOE
#define SPI4_ACC_MISO_Pin GPIO_PIN_5
#define SPI4_ACC_MISO_GPIO_Port GPIOE
#define SPI4_ACC_MOSI_Pin GPIO_PIN_6
#define SPI4_ACC_MOSI_GPIO_Port GPIOE
#define ACC_READY_Pin GPIO_PIN_0
#define ACC_READY_GPIO_Port GPIOF
#define SPI4_ACC_CS2_Pin GPIO_PIN_1
#define SPI4_ACC_CS2_GPIO_Port GPIOF
#define SPI4_ACC_CS1_Pin GPIO_PIN_2
#define SPI4_ACC_CS1_GPIO_Port GPIOF
#define ACC_STROBE_Pin GPIO_PIN_3
#define ACC_STROBE_GPIO_Port GPIOF
#define MB_PROGRAM_Pin GPIO_PIN_4
#define MB_PROGRAM_GPIO_Port GPIOF
#define PROGRAM_B_Pin GPIO_PIN_5
#define PROGRAM_B_GPIO_Port GPIOF
#define INIT_B_Pin GPIO_PIN_6
#define INIT_B_GPIO_Port GPIOF
#define SPI5_DB_SCK_Pin GPIO_PIN_7
#define SPI5_DB_SCK_GPIO_Port GPIOF
#define SPI5_DB_MISO_Pin GPIO_PIN_8
#define SPI5_DB_MISO_GPIO_Port GPIOF
#define SPI5_DB_MOSI_Pin GPIO_PIN_9
#define SPI5_DB_MOSI_GPIO_Port GPIOF
#define RCC_OSC_OUT_UNUSED_Pin GPIO_PIN_1
#define RCC_OSC_OUT_UNUSED_GPIO_Port GPIOH
#define LED_YELLOW_Pin GPIO_PIN_0
#define LED_YELLOW_GPIO_Port GPIOB
#define MB_FPGA_0_Pin GPIO_PIN_0
#define MB_FPGA_0_GPIO_Port GPIOG
#define MB_FPGA_1_Pin GPIO_PIN_1
#define MB_FPGA_1_GPIO_Port GPIOG
#define SPI5_DB_CS2_1_Pin GPIO_PIN_7
#define SPI5_DB_CS2_1_GPIO_Port GPIOE
#define SPI5_DB_CS2_2_Pin GPIO_PIN_8
#define SPI5_DB_CS2_2_GPIO_Port GPIOE
#define SPI5_DB_CS2_3_Pin GPIO_PIN_9
#define SPI5_DB_CS2_3_GPIO_Port GPIOE
#define SPI5_DB_CS2_4_Pin GPIO_PIN_10
#define SPI5_DB_CS2_4_GPIO_Port GPIOE
#define SPI5_DB_CS2_5_Pin GPIO_PIN_11
#define SPI5_DB_CS2_5_GPIO_Port GPIOE
#define SPI5_DB_CS2_6_Pin GPIO_PIN_12
#define SPI5_DB_CS2_6_GPIO_Port GPIOE
#define USART3_RX_UNUSED_Pin GPIO_PIN_11
#define USART3_RX_UNUSED_GPIO_Port GPIOB
#define LED_RED_Pin GPIO_PIN_14
#define LED_RED_GPIO_Port GPIOB
#define MB_FPGA_2_Pin GPIO_PIN_2
#define MB_FPGA_2_GPIO_Port GPIOG
#define MB_FPGA_3_Pin GPIO_PIN_3
#define MB_FPGA_3_GPIO_Port GPIOG
#define MB_FPGA_4_Pin GPIO_PIN_4
#define MB_FPGA_4_GPIO_Port GPIOG
#define MB_FPGA_5_Pin GPIO_PIN_5
#define MB_FPGA_5_GPIO_Port GPIOG
#define MB_FPGA_6_Pin GPIO_PIN_6
#define MB_FPGA_6_GPIO_Port GPIOG
#define MB_FPGA_7_Pin GPIO_PIN_7
#define MB_FPGA_7_GPIO_Port GPIOG
#define I2C3_DB_SDA_Pin GPIO_PIN_9
#define I2C3_DB_SDA_GPIO_Port GPIOC
#define I2C3_DB_SCL_Pin GPIO_PIN_8
#define I2C3_DB_SCL_GPIO_Port GPIOA
#define SPI5_DB_CS1_Pin GPIO_PIN_0
#define SPI5_DB_CS1_GPIO_Port GPIOD
#define SPI5_DB_CS2_Pin GPIO_PIN_1
#define SPI5_DB_CS2_GPIO_Port GPIOD
#define SPI5_DB_CS3_Pin GPIO_PIN_2
#define SPI5_DB_CS3_GPIO_Port GPIOD
#define SPI5_DB_CS4_Pin GPIO_PIN_3
#define SPI5_DB_CS4_GPIO_Port GPIOD
#define SPI5_DB_CS5_Pin GPIO_PIN_4
#define SPI5_DB_CS5_GPIO_Port GPIOD
#define SPI5_DB_CS6_Pin GPIO_PIN_5
#define SPI5_DB_CS6_GPIO_Port GPIOD
#define SPI5_MB_CS0_Pin GPIO_PIN_6
#define SPI5_MB_CS0_GPIO_Port GPIOD
#define SPI5_MB_CS7_Pin GPIO_PIN_7
#define SPI5_MB_CS7_GPIO_Port GPIOD
#define I2C1_MB_SCL_Pin GPIO_PIN_6
#define I2C1_MB_SCL_GPIO_Port GPIOB
#define LED_BLUE_Pin GPIO_PIN_7
#define LED_BLUE_GPIO_Port GPIOB
#define I2C1_MB_SDA_Pin GPIO_PIN_9
#define I2C1_MB_SDA_GPIO_Port GPIOB
#define GO_IN_Pin GPIO_PIN_0
#define GO_IN_GPIO_Port GPIOE
#define FAN_FAIL_Pin GPIO_PIN_1
#define FAN_FAIL_GPIO_Port GPIOE
/* USER CODE BEGIN Private defines */

/* USER CODE END Private defines */

#ifdef __cplusplus
}
#endif

#endif /* __MAIN_H */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
