#pragma once

// 2.0.0 - Booted. Telnet. TFTP to scratch.
// 2.0.1 - Fixed memory leak in telnet.
// 2.0.2 - Can write a file to the app sectors of flash.
// 2.0.3 - Can boot application from flash.
// 2.0.4 - MD5 for application image.
// 2.0.5 - Limit size of MD5 image.

#define BSL_VERSION "2.0.5"
