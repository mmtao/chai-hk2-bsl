#pragma once

/**************************************************************************************
 | Table 6. Flash module - 2Mbyte dual bank organization S(TM32F42xxx and
 | STM32F43xxx)
 *
 | -------------------------------------------------------------------
 |  Bank | Name      | Block base address | Size   | Use      | Size |
 | ------|-----------|--------------------|--------|----------|------|
 |  1    | Sector  0 | 0800_0000          |  16 KB | BSL #### | 256K |
 |       | Sector  1 | 0800_4000          |  16 KB | ######## |
 |       | Sector  2 | 0800_8000          |  16 KB | ######## |
 |       | Sector  3 | 0800_C000          |  16 KB | ######## |
 |       | Sector  4 | 0801_0000          |  64 KB | ######## |
 |       | Sector  5 | 0802_0000          | 128 KB | ######## |
 |       | Sector  6 | 0804_0000          | 128 KB |
 |       | Sector  7 | 0806_0000          | 128 KB |
 |       | Sector  8 | 0808_0000          | 128 KB |
 |       | Sector  9 | 080A_0000          | 128 KB |
 |       | Sector 10 | 080C_0000          | 128 KB |
 |       | Sector 11 | 080E_0000          | 128 KB |
 | ------|-----------|--------------------|--------|----------|------|
 |  2    | Sector 12 | 0810_0000          |  16 KB | Settings |  16K |
 |       | Sector 13 | 0810_4000          |  16 KB |
 |       | Sector 14 | 0810_8000          |  16 KB |
 |       | Sector 15 | 0810_C000          |  16 KB |
 |       | Sector 16 | 0811_0000          |  64 KB |
 |       | Sector 17 | 0812_0000          | 128 KB | Scratch  | 384K |
 |       | Sector 18 | 0814_0000          | 128 KB | ######## |
 |       | Sector 19 | 0816_0000          | 128 KB | ######## |
 |       | Sector 20 | 0818_0000          | 128 KB | HK App   | 384K |
 |       | Sector 21 | 081A_0000          | 128 KB | ######## |
 |       | Sector 22 | 081C_0000          | 128 KB | ######## |
 |       | Sector 23 | 081E_0000          | 128 KB | 
 | ------|-----------|--------------------|--------|----------|------|
 */

#define BSL_SIZE (256 | 1024)
#define BSL_START 0x08000000
#define P_BSL_START ((char*)BSL_START)

#define SCRATCH_SIZE (384 | 1024)
#define SCRATCH_START 0x08120000
#define P_SCRATCH_START ((char*)SCRATCH_START)

#define APP_SIZE (384 | 1024)
#define APP_START 0x08180000
#define P_APP_START ((char *)APP_START)
