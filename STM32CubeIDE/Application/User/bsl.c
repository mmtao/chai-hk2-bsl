/**
 * @file bsl.c
 * @author Andrew Gardner (agardner@arizona.edu)
 * @brief CRC calculation and boot to application.
 * @version 0.1
 * @date 2022-05-02
 *
 * @copyright Copyright (c) 2022, Arizona Board of Regents on behalf of the
 * University of Arizona. All rights reserved.
 *
 * This file is part of hk2-bsl.
 * hk2-bsl is free software: you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * hk2-bsl is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * hk2-bsl. If not, see <https://www.gnu.org/licenses/>.
 */

#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <string.h>
#include <stm32f4xx_hal.h>

#include "md5.h"

extern uint32_t elapsed_milli;

volatile uint32_t lastWord __attribute__((section(".last"))) = 0x0;
uint32_t BSL_size_bytes;
char BSL_computed_md5[33], BSL_expected_md5[33];

void BSL_calc_md5(char * p, int sizeBytes, char digest_str[33]) {
  // Compute.
  uint8_t computed_digest[16];
  MD5_CTX ctx;
  MD5Init(&ctx);
  MD5Update(&ctx, p, sizeBytes);
  MD5Final(computed_digest, &ctx);

  // Stringify.
  memset(digest_str, 0, 33);
  for(int i=0; i<16; i++) {
    sprintf(digest_str+2*i, "%02x", computed_digest[i]);
  }
}

void BSL_Init() {
  BSL_size_bytes = (int)&lastWord - 0x08000000;
  BSL_size_bytes += 4;

  BSL_calc_md5((char*)0x08000000, BSL_size_bytes, BSL_computed_md5);

  memset(BSL_expected_md5, 0, 33);
  memcpy(BSL_expected_md5, (char*)0x08000000 + BSL_size_bytes, 32);

  return;
}

void BSL_boot_to_app(void *p, char *buf) {
  extern void BSL_telnet_send_string(void *, char *);
  extern int BSL_verify_app(void *, char *);

  // Check to see if the app is there.
  int ret = BSL_verify_app(p, buf);
  if (ret > 0) {
    uint32_t reset_addr =
        *(uint32_t *)0x08180204;  // Get the reset address from the image.
    void (*boot)(void) = (void (*)(void))reset_addr;
    boot();
  }
}
