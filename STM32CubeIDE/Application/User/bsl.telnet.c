/**
 * @file bsl.telnet.c
 * @author Andrew Gardner (agardner@arizona.edu)
 * @brief Telnet server for BSL.
 * @version 0.1
 * @date 2022-05-02
 *
 * @copyright Copyright (c) 2022, Arizona Board of Regents on behalf of the
 * University of Arizona. All rights reserved.
 *
 * This file is part of hk2-bsl.
 * hk2-bsl is free software: you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * hk2-bsl is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * hk2-bsl. If not, see <https://www.gnu.org/licenses/>.
 */

#include <stdbool.h>
#include <stdint.h>
#include <string.h>

#include <lwip/opt.h>
#include <lwip/debug.h>
#include <lwip/stats.h>
#include <lwip/tcp.h>

#include "bsl.version.h"

extern uint32_t BSL_boot_elapsed_milli;
extern char * BSL_computed_md5;
extern char * BSL_expected_md5;

enum trace {
  INIT = 1,
  ACCEPT = 2,
  SEND_STRING = 3,
  RECV = 4,
  POLL = 5,
  SENT = 6,
  SEND = 7,
  ERR = 8,
  CLOSE = 9,
  CHECK = 10,
  SEND_ERR = 11,
};
enum trace trace_buf[256];
uint32_t trace_idx = 0;
#define TRACE(X)            \
  trace_buf[trace_idx] = X; \
  trace_idx = (trace_idx + 1) % 256;

////////////////////////////////////////
// BSL Telnet interface.

static char greeting[] = "HK BSL 2\n",
            bsl_version[] = "Version " BSL_VERSION "\n", prompt[] = "$ ",
            green[] = "\033[1;32m", reset[] = "\033[0m", newline[] = "\n",
            red[] = "\033[1;31m";

static char current_command[32];

static struct tcp_pcb *telnet_pcb;

#define STATE_NONE 0
#define STATE_ACCEPTED 1
#define STATE_RECEIVED 2
#define STATE_CLOSING 3

typedef struct {
  struct tcp_pcb *pcb;
  struct pbuf *p;
  uint8_t state, retries;
} telnet_state_t;

// There is only one connection. Behavior with more than one connection
// is undefined and probably very terrible.
static telnet_state_t ts;
static char output_buffer[128];

int BSL_verify_scratch(struct tcp_pcb *pcb, char *outbuf);
int BSL_verify_app(struct tcp_pcb *p_pcb, char *output_buffer);
void BSL_burn_scratch_to_app(struct tcp_pcb *pcb, char *outbuf);
void BSL_telnet_send_string(struct tcp_pcb *p_pcb, char *c);

static void BSL_telnet_check_command(struct tcp_pcb *p_pcb) {
  TRACE(CHECK)
  int len = strlen(current_command);

  bool cmd_in_line_mode =
      current_command[len - 2] == '\r' && current_command[len - 1] == '\n';
  bool cmd_in_char_mode = current_command[len - 1] == '\r';

  if (cmd_in_char_mode) {
    BSL_telnet_send_string(p_pcb, newline);
  }

  ////////////////////////////////////////
  // COMMANDS:
  // "crc"     : Print the expected and computed CRCs for the running object.
  // "quit"    : End this telnet session.
  // "verifys" : Verify the contents of the scratch partition.
  // "verifya" : Verify the contents of the scratch partition.
  // "burn"    : Copy the (reverified) file from scratch to the app image slot.

  if (cmd_in_line_mode || cmd_in_char_mode) {
    // Check for commands we know.
    if (0 == strncmp(current_command, "md5", 3)) {
      //////////
      // MD5
      sprintf(output_buffer, "Expected: %s\nComputed: %s\n",
              BSL_expected_md5, BSL_computed_md5);
      BSL_telnet_send_string(p_pcb, output_buffer);
    } else if (0 == strncmp(current_command, "quit", 4)) {
      //////////
      // QUIT
      ts.state = STATE_CLOSING;
    } else if (0 == strncmp(current_command, "verifys", 7)) {
      //////////
      // Verify scratch
      BSL_verify_scratch(p_pcb, output_buffer);
    } else if (0 == strncmp(current_command, "verifya", 7)) {
      //////////
      // Verify app
      BSL_verify_app(p_pcb, output_buffer);
    } else if (0 == strncmp(current_command, "burn", 4)) {
      //////////
      // Burn.
      BSL_burn_scratch_to_app(p_pcb, output_buffer);
    } else if (0 == strncmp(current_command, "boot", 4)) {
      void BSL_boot_to_app(void *p_pcb, char *);
      BSL_boot_to_app(p_pcb, output_buffer);
    } else {
      sprintf(output_buffer, "%sUnknown command.%s\n", red, reset);
      BSL_telnet_send_string(p_pcb, output_buffer);
    }

    BSL_telnet_send_string(p_pcb, prompt);

    memset(current_command, 0, sizeof(current_command));
  }
}

static void BSL_telnet_close(struct tcp_pcb *p_pcb, telnet_state_t *p_state) {
  TRACE(CLOSE)
  tcp_arg(p_pcb, NULL);
  tcp_sent(p_pcb, NULL);
  tcp_recv(p_pcb, NULL);
  tcp_err(p_pcb, NULL);
  tcp_poll(p_pcb, NULL, 0);
  tcp_close(p_pcb);

  memset(p_state, 0, sizeof(ts));
}

static void BSL_telnet_err(void *arg __attribute__((unused)),
                           err_t err __attribute__((unused))) {
  // Something bad happened. Reset connections state.
  TRACE(ERR)
  memset(&ts, 0, sizeof(ts));
}

static err_t send_err = 0;

static void BSL_telnet_send(struct tcp_pcb *p_pcb, telnet_state_t *p_state) {
  TRACE(SEND)
  err_t err = ERR_OK;
  while ((err == ERR_OK) && (p_state->p != NULL) &&
         (p_state->p->len <= tcp_sndbuf(p_pcb))) {
    // Queue data for xmit.
    err = tcp_write(p_pcb, p_state->p->payload, p_state->p->len,
                    TCP_WRITE_FLAG_COPY);
    if (err == ERR_OK) {
      // Remember the current buffer.
      struct pbuf *ptr = p_state->p;
      uint16_t len = ptr->len;

      // Get ready for the next.
      p_state->p = ptr->next;
      if (p_state->p != NULL) {
        // pbuf_ref(p_state->p);
      }

      // Forget the data that's sent.
      pbuf_free(ptr);
      tcp_recved(p_pcb, len);
    } else {
      TRACE(SEND_ERR)
      send_err = err;
    }
  }
}

static err_t BSL_telnet_sent(void *arg, struct tcp_pcb *p_pcb, u16_t len) {
  TRACE(SENT)
  telnet_state_t *p_state = (telnet_state_t *)arg;
  p_state->retries = 0;

  if (p_state->p != NULL) {
    // tcp_sent(p_pcb, BSL_telnet_sent);
    BSL_telnet_send(p_pcb, p_state);
  } else if (p_state->state == STATE_CLOSING) {
    BSL_telnet_close(p_pcb, p_state);
  }

  return ERR_OK;
}

static err_t BSL_telnet_poll(void *arg, struct tcp_pcb *p_pcb) {
  TRACE(POLL)
  telnet_state_t *p_state = (telnet_state_t *)arg;

  // If there's no state object, abort.
  if (p_state == NULL) {
    tcp_abort(p_pcb);
    return ERR_ABRT;
  }

  // If there is something to process, do that.
  if (p_state->p != NULL) {
    BSL_telnet_send(p_pcb, p_state);
  } else if (p_state->state == STATE_CLOSING) {
    BSL_telnet_close(p_pcb, p_state);
  }

  return ERR_OK;
}

static err_t BSL_telnet_recv(void *arg, struct tcp_pcb *p_pcb, struct pbuf *p,
                             err_t err) {
  TRACE(RECV)
  telnet_state_t *p_state = (telnet_state_t *)arg;

  // Did the remote host close the connection?
  if (p == NULL) {
    p_state->state = STATE_CLOSING;
    if (p_state->p == NULL) {
      BSL_telnet_close(p_pcb, p_state);
    } else {
      BSL_telnet_send(p_pcb, p_state);
    }
    return ERR_OK;
  }

  // Did some other kind of weird error happen?
  if (err != ERR_OK) {
    pbuf_free(p);
    return err;
  }

  static uint8_t *q;

  // We didn't find errors, so process based on our state.
  switch (p_state->state) {
    case STATE_ACCEPTED:  // Connection established.
      memset(current_command, 0, sizeof(current_command));
      p_state->state = STATE_RECEIVED;
      return ERR_OK;
    case STATE_RECEIVED:  // Now we're getting data.
                          // We might overrun the buffer, and that's not good,
                          // but we aren't going to deal with that here.
      q = (uint8_t *)p->payload;
      if (q[0] != 255) {
        strncat(current_command, p->payload, p->tot_len);
        BSL_telnet_check_command(p_pcb);
      }
      tcp_recved(p_pcb, p->tot_len);
      pbuf_free(p);
      return ERR_OK;
    default:
      // We didn't find an error, but our state isn't right to be here. Try to
      // clean up.
      tcp_recved(p_pcb, p->tot_len);
      pbuf_free(p);
      return ERR_OK;
  }

  return ERR_OK;
}

void BSL_telnet_send_string(struct tcp_pcb *p_pcb, char *c) {
  TRACE(SEND_STRING)
  // Set up the packet buffer.
  struct pbuf *p = pbuf_alloc(PBUF_TRANSPORT, strlen(c), PBUF_RAM);
  pbuf_take(p, c, strlen(c));

  // Queue the packet buffer.
  if (ts.p == NULL) {
    ts.p = p;
    BSL_telnet_send(p_pcb, &ts);
  } else {
    pbuf_cat(ts.p, p);
  }
}

// A new connection is accepted.
static err_t BSL_telnet_accept(void *arg __attribute__((unused)),
                               struct tcp_pcb *p_pcb, err_t err) {
  TRACE(ACCEPT)
  extern bool BSL_boot;
  BSL_boot = false;

  // Check params.
  if (err != ERR_OK || p_pcb == NULL) {
    return ERR_VAL;
  }

  memset(current_command, 0, sizeof(current_command));

  ts.state = STATE_ACCEPTED;
  ts.pcb = p_pcb;
  ts.retries = 0;
  ts.p = NULL;

  tcp_arg(p_pcb, &ts);
  tcp_recv(p_pcb, BSL_telnet_recv);
  tcp_err(p_pcb, BSL_telnet_err);
  tcp_poll(p_pcb, BSL_telnet_poll, 0);
  tcp_sent(p_pcb, BSL_telnet_sent);

  static uint8_t options[] = {255, 251, 34, 0};  // Line mode, please.
  BSL_telnet_send_string(p_pcb, (char *)options);

  sprintf(output_buffer, "%s%s%sBooted in %ld ms\n%s%s", green, greeting,
          bsl_version, BSL_boot_elapsed_milli, reset, prompt);
  BSL_telnet_send_string(p_pcb, output_buffer);

  return ERR_OK;
}

void BSL_Telnet_Init() {
  TRACE(INIT)
  telnet_pcb = tcp_new_ip_type(IPADDR_TYPE_ANY);
  if (telnet_pcb == NULL) {
    return;
  }

  err_t err = tcp_bind(telnet_pcb, IP_ANY_TYPE, 23);
  if (err != ERR_OK) {
    return;
  }

  telnet_pcb = tcp_listen(telnet_pcb);
  tcp_accept(telnet_pcb, BSL_telnet_accept);
}
