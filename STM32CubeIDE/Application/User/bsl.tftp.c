/**
 * @file bsl.tftp.c
 * @author Andrew Gardner (agardner@arizona.edu)
 * @brief TFTP server for BSL.
 * @version 0.1
 * @date 2022-05-02
 *
 * @copyright Copyright (c) 2022, Arizona Board of Regents on behalf of the
 * University of Arizona. All rights reserved.
 *
 * This file is part of hk2-bsl.
 * hk2-bsl is free software: you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * hk2-bsl is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * hk2-bsl. If not, see <https://www.gnu.org/licenses/>.
 */

#include <stdbool.h>
#include <string.h>
#include <lwip/apps/tftp_server.h>
#include "bsl.map.h"
#include <stm32f4xx_hal_flash.h>

    extern uint32_t BSL_size_bytes;

typedef struct {
  char *p_start;
  int offset, total;
} read_status_t;
static read_status_t read_status;

typedef struct {
  uint32_t start;
  int offset, total;
} write_status_t;
static write_status_t write_status;

static void tftp_close(void *handle) {
  memset(handle, 0, sizeof(read_status_t));
  HAL_FLASH_Lock();
  return;
}

static FLASH_EraseInitTypeDef eraseScratch = {
    // Erase the scratch partition.
    .TypeErase = FLASH_TYPEERASE_SECTORS,
    .VoltageRange = FLASH_VOLTAGE_RANGE_3,
    .Sector = FLASH_SECTOR_17,
    .NbSectors = 3,
    .Banks = FLASH_BANK_2};

static void *tftp_open(const char *fname, const char *mode, uint8_t is_write) {
  extern bool BSL_boot;
  BSL_boot = false;

  uint32_t err = 0;
  if (is_write == 1) {
    // Set up to write to scratch.
    write_status.start = SCRATCH_START;
    write_status.offset = write_status.total = 0;
    // Erase the scratch blocks.
    HAL_FLASH_Unlock();
    HAL_FLASHEx_Erase(&eraseScratch, &err);
    return (void *)&write_status;
  } else {
    // Download something from flash.
    if (0 == strncmp(fname, "hk2-bsl.bin", 11)) {
      read_status.p_start = P_BSL_START;
      read_status.offset = 0;
      read_status.total = BSL_size_bytes + 4;  // Add 4 for the CRC.
      return (void *)&read_status;
    } else if (0 == strncmp(fname, "scratch.bin", 11)) {
      read_status.p_start = P_SCRATCH_START;
      read_status.offset = 0;
      read_status.total = SCRATCH_SIZE;
      return (void *)&read_status;
    } else if (0 == strncmp(fname, "app.bin", 7)) {
      read_status.p_start = P_APP_START;
      read_status.offset = 0;
      read_status.total = APP_SIZE;
      return (void *)&read_status;
    }
  }

  return NULL;
}

static int tftp_read(void *handle, void *buf, int bytes) {
  int remaining = read_status.total - read_status.offset;
  int bytes_to_write = remaining < bytes ? remaining : bytes;
  memcpy(buf, read_status.p_start + read_status.offset, bytes_to_write);
  read_status.offset += bytes_to_write;
  return bytes_to_write;
}

static int tftp_write(void *handle, struct pbuf *p) {
  int i;

  uint8_t *q = (uint8_t *)p->payload;
  for (i = 0; i < p->len; i++) {
    // Write a single bytes.
    HAL_FLASH_Program(FLASH_TYPEPROGRAM_BYTE,
                      write_status.start + write_status.offset, q[i]);
    write_status.offset++;
  }

  return 0;
}

void BSL_Tftp_Init() {
  static const struct tftp_context ctx = {
      .close = tftp_close,
      .open = tftp_open,
      .read = tftp_read,
      .write = tftp_write,
  };
  tftp_init(&ctx);
}
