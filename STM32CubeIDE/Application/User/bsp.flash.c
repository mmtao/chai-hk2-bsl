/**
 * @file bsl.flash.c
 * @author Andrew Gardner (agardner@arizona.edu)
 * @brief Write and verify applications in internal flash.
 * @version 0.1
 * @date 2022-05-02
 *
 * @copyright Copyright (c) 2022, Arizona Board of Regents on behalf of the
 * University of Arizona. All rights reserved.
 *
 * This file is part of hk2-bsl.
 * hk2-bsl is free software: you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * hk2-bsl is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * hk2-bsl. If not, see <https://www.gnu.org/licenses/>.
 */

#include <stdbool.h>
#include <stdint.h>
#include <string.h>

#include <lwip/opt.h>
#include <lwip/debug.h>
#include <lwip/stats.h>
#include <lwip/tcp.h>

#include "bsl.map.h"
#include "bsl.version.h"

static char green[] = "\033[1;32m", reset[] = "\033[0m", red[] = "\033[1;31m";

typedef struct {
    char filename[100];
    char mode[8], oid[8], gid[8];
    char size[12];
    char mtime[12];
    char checksum[8];
    char link;
    char linkname[100];
    char ustar[6];
    char version[2];
    char uname[32];
    char gname[32];
    char major[8];
    char minor[8];
    char prefix[155];
    char pad[12];
} ustar_t;

#define SIZE(x) strtol(x->size, NULL, 8)

void BSL_telnet_send_string(struct tcp_pcb *p_pcb, char *c);
int BSL_verify_scratch(struct tcp_pcb *p_pcb, char *output_buffer);
int BSL_verify_app(struct tcp_pcb *p_pcb, char *output_buffer);
void BSL_calc_md5(char * p, int sizeBytes, char digest_str[33]);

int BSL_verify_image(struct tcp_pcb *p_pcb, char *output_buffer, char * start_addr) {
  // Scratch should contain a tar file.  The first test is to see
  // if we can find the "ustar" string at a 257 byte offset.
  ustar_t * u = (ustar_t *) start_addr;
  if(0 != strncmp(u->ustar, "ustar", 5)) {
    sprintf(output_buffer, "File is not in Unix Standard TAR format.\n");
    BSL_telnet_send_string(p_pcb, output_buffer);
    return -1;
  }

  int u_size = SIZE(u);
  char expected_md5[33];
  ustar_t * v = (ustar_t *) (start_addr + 512 + u_size + (512 - (u_size % 512)));
  int v_size = SIZE(v);
  strncpy(expected_md5, (char*)(v+1) , 32); // Start + tar header + size + round up + tar header.
  sprintf(output_buffer, "\tImage file: '%s', %d bytes. MD5=%s\n", u->filename,
          u_size, expected_md5);
  BSL_telnet_send_string(p_pcb, output_buffer);

  char computed_md5[33];
  BSL_calc_md5((char*)(u+1), u_size, computed_md5);

  if (0 == strncmp(expected_md5, computed_md5, 32)) {
    sprintf(output_buffer, "\tComputed MD5: %s%s. Image verified.%s\n", green, computed_md5, reset);
    BSL_telnet_send_string(p_pcb, output_buffer);

    // Return total bytes = (tar)(bin)(pad) + (tar)(sum)(pad)
    return (512 + u_size + (512 - (u_size % 512))) + (512 + v_size + (512 - (v_size % 512)));
  } else {
    sprintf(output_buffer, "\tComputed MD5: %s%s. Image NOT verified.%s\n", red, computed_md5, reset);
    BSL_telnet_send_string(p_pcb, output_buffer);
    return -1;
  }
}

static FLASH_EraseInitTypeDef eraseApp = {
    // Erase the scratch partition.
    .TypeErase = FLASH_TYPEERASE_SECTORS,
    .VoltageRange = FLASH_VOLTAGE_RANGE_3,
    .Sector = FLASH_SECTOR_20,
    .NbSectors = 3,
    .Banks = FLASH_BANK_2};

void BSL_burn_scratch_to_app(struct tcp_pcb *p_pcb, char *output_buffer) {
  uint32_t err;

  // Verify the image, one more time.
  int sizeBytes = BSL_verify_scratch(p_pcb, output_buffer);
  if (sizeBytes > 0) {
    HAL_FLASH_Unlock();
    HAL_FLASHEx_Erase(&eraseApp, &err);
    char *p = P_SCRATCH_START;
    for (int i = 0; i < sizeBytes; i++) {
      HAL_FLASH_Program(FLASH_TYPEPROGRAM_BYTE, APP_START + i, p[i]);
    }
    HAL_FLASH_Lock();
    sprintf(output_buffer, "Wrote %d bytes.\n", sizeBytes);
    BSL_telnet_send_string(p_pcb, output_buffer);
  }

  BSL_verify_app(p_pcb, output_buffer);
}

int BSL_verify_scratch(struct tcp_pcb *p_pcb, char *output_buffer) {
	sprintf(output_buffer, "Verifying scratch...\n");
	BSL_telnet_send_string(p_pcb, output_buffer);
	return BSL_verify_image(p_pcb, output_buffer, P_SCRATCH_START);
}

int BSL_verify_app(struct tcp_pcb *p_pcb, char *output_buffer) {
	sprintf(output_buffer, "Verifying app...\n");
	BSL_telnet_send_string(p_pcb, output_buffer);
	return BSL_verify_image(p_pcb, output_buffer, P_APP_START);
}
