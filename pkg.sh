#!/bin/bash
set -x otrace

md5sum hk2-bsl.bin | tee hk2-bsl.md5sum
cp hk2-bsl.bin hk2-bsl.flash
cat hk2-bsl.md5sum >> hk2-bsl.flash
