/**
 * Parse the housekeeper build products to put the size of the executable in
 * flash into the executable so that it can compute it's own CRC.
 * Then report the CRC with the size in the modified executable.
 */

#include <sys/mman.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <arpa/inet.h>
#include <errno.h>
#include <fcntl.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

static uint32_t calculateWord(uint32_t crc, uint32_t data);
static uint32_t calcSTM32CRC(uint32_t *buffer, uint32_t count);

int main(int argc, char *argv[]) {
  int fd = open(argv[1], O_RDONLY);
  if (fd < 0) {
    perror("open");
    return -errno;
  }

  struct stat s;
  int retv = fstat(fd, &s);
  if (retv < 0) {
    perror("fstat");
    return -errno;
  }

  uint32_t *p =
      mmap(NULL, s.st_size, PROT_READ | PROT_WRITE, MAP_PRIVATE, fd, 0);
  if (p == MAP_FAILED) {
    perror("mmap");
    return -errno;
  }

  uint32_t sizeWords = (s.st_size / 4) - 1;
  uint32_t crc = calcSTM32CRC(p, sizeWords);
  printf("%08x %d %d\n", crc, crc, sizeWords);

  p[sizeWords] = crc;

  munmap(p, s.st_size);
  close(fd);

  return 0;
}

static uint32_t calculateWord(uint32_t crc, uint32_t data) {
  // printf("%08x ", data);

  crc = crc ^ data;

  int i;
  for (i = 0; i < 32; i++) {
    if ((crc & 0x80000000) != 0) {
      crc = (crc << 1) ^ 0x04C11DB7;  // Polynomial used in STM32
    } else {
      crc = (crc << 1);
    }
  }

  // printf("%08x\n", crc);
  return (crc);
}

static uint32_t calcSTM32CRC(uint32_t *buffer, uint32_t count) {
  uint32_t crc = 0xFFFFFFFF;

  for (int i = 0; i < count; i++) {
    // printf("%d ", i);
    if (i != 107) {
      crc = calculateWord(crc, buffer[i]);
    }
  }

  return (crc);
}
